use logos::{Lexer, Logos, Source};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    #[regex(
        r#"<a[^h]+href="([^"]*)"[^>]*>([^<]*)</a[ \t\n\f]*>"#,
        extract_link_info
    )]
    Link((LinkUrl, LinkText)),

    #[regex(r"<[^>]*>", logos::skip)]
    #[regex(r"[^<]+", logos::skip)]
    Ignored,

    // Catch any error
    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    let slice = lex.slice();

    let href_start = slice.find("href=\"").unwrap() + 6;
    let href_end = href_start + slice[href_start..].find("\"").unwrap();
    let href = slice[href_start..href_end].to_string();

    let text_start = slice.find(">").unwrap() + 1;
    let text_end = text_start + slice[text_start..].find("<").unwrap();
    let text = slice[text_start..text_end].to_string();

    (LinkUrl(href), LinkText(text))
}
